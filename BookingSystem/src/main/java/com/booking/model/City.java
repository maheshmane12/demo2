package com.booking.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class City {

	@Id
	@GeneratedValue
	private int c_id;
	private String name;
	
	@ManyToOne
	private BookForm book;

	public City() {
		super();
	}

	public City(int c_id, String name, BookForm book) {
		super();
		this.c_id = c_id;
		this.name = name;
		this.book=book;
	}

	public int getC_id() {
		return c_id;
	}

	public void setC_id(int c_id) {
		this.c_id = c_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public BookForm getBook(BookForm book) {
		return book;
	}
	
	public void setBook() {
		this.book=book;
	}

	@Override
	public String toString() {
		return "City [c_id=" + c_id + ", name=" + name + ", book=" +book+ "]";
	}

}

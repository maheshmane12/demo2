package com.booking.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="bookform")
public class BookForm {

	@Id
	@GeneratedValue
	private int b_id;
	private String f_name;
	private String l_name;
	private int cityid;
	private String d_to;
	private String contact;
	private String compartment;
	
	@OneToMany(mappedBy = "book")
	private City city;

	public BookForm() {
		super();
	}

	public BookForm(int b_id, String f_name, String l_name, int cityid, String d_to, String contact,
			String compartment) {
		super();
		this.b_id = b_id;
		this.f_name = f_name;
		this.l_name = l_name;
		this.cityid = cityid;
		this.d_to = d_to;
		this.contact = contact;
		this.compartment = compartment;
	}


	public int getB_id() {
		return b_id;
	}

	public void setB_id(int b_id) {
		this.b_id = b_id;
	}

	public String getF_name() {
		return f_name;
	}

	public void setF_name(String f_name) {
		this.f_name = f_name;
	}

	public String getL_name() {
		return l_name;
	}

	public void setL_name(String l_name) {
		this.l_name = l_name;
	}
	
	public City getCity() {
		return city;
	} 

	public void setCity(City city) {
		this.city = city;
	}

	public int getcityid() {
		return cityid;
	}

	public void setcityid(int cityid) {
		this.cityid = cityid;
	}

	public String getD_to() {
		return d_to;
	}

	public void setD_to(String d_to) {
		this.d_to = d_to;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getCompartment() {
		return compartment;
	}

	public void setCompartment(String compartment) {
		this.compartment = compartment;
	}

	@Override
	public String toString() {
		return "BookForm [b_id=" + b_id + ", f_name=" + f_name + ", l_name=" + l_name + ", cityid=" + cityid + ", d_to="
				+ d_to + ", contact=" + contact + ", compartment=" + compartment + "]";
	}

}

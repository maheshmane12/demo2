package com.booking.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.booking.model.BookForm;
import com.booking.model.Getcityid;

public class BookingDao {
	Connection con = null;
	
	public boolean uservalid(String username, String password) {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			 con=DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/bookingsystem", "root", "password");
		
			 Statement st = con.createStatement();
			 
			 String sql=(" select username, password from register where username='"+username+"' and password='"+password+"'");
				ResultSet rs = st.executeQuery(sql);
				if(rs.next()) {
					return true;
				}
				
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		return false;
	}

	public String insertdata(String f_name, String l_name, String cityid, String d_to, String contact, String compartment)
		{
			List<Getcityid> cityId = new ArrayList<Getcityid>();
			 
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				 con=DriverManager.getConnection(
						"jdbc:mysql://localhost:3306/bookingsystem", "root", "password");
			
				 Statement st = con.createStatement();
				 
				 String sql=(" select c_id from city where cityname='"+cityid+"'");
					ResultSet rs = st.executeQuery(sql);
					while(rs.next()) 
					{
						Getcityid getcity = new Getcityid();
						getcity.setId(rs.getString(1));
						cityId.add(getcity);
					}
						
					 String id =cityId.get(0).getId();
					
		 int insert = st.executeUpdate("insert into bookform(f_name, l_name, cityid, d_to, contact, compartment)"
		 + " values('"+f_name+"','"+l_name+"','"+id+"','"+d_to+"','"+contact+"','"+compartment+"')");
						
			} catch (Exception e) {
				e.printStackTrace();
			} 
			
			return "";
		}
	
	
	public String register(String f_name, String l_name, String username, String password, String email)
	{
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			 con=DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/bookingsystem", "root", "password");
		
			 Statement st = con.createStatement();
			 
			
				
	 int insert = st.executeUpdate("insert into register(f_name, l_name, username, password, email)"
	 + " values('"+f_name+"','"+l_name+"','"+username+"','"+password+"','"+email+"')");
					
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		return "";
	}

	public String change(String b_id, String f_name, String l_name, String cityid, String d_to, String contact,
			String compartment) {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/bookingsystem", "root", "password");

			Statement st = con.createStatement();

			int update = st.executeUpdate("update bookform set f_name='" + f_name + "', l_name='" + l_name
					+ "', cityid='" + cityid + "', d_to='" + d_to + "', contact='" + contact + "', compartment='"
					+ compartment + "' where b_id= '" + b_id + "' ");

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "";
	}

	public String delete(String b_id) {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/bookingsystem", "root", "password");

			Statement st = con.createStatement();
			int delete = st.executeUpdate("delete from bookform where b_id='"+b_id+"'");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "";
	}
	
	public List<BookForm> display() {
		
		List<BookForm> book = new ArrayList<BookForm>();
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/bookingsystem", "root", "password");

			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery("select * from bookform");
			
			
			while(rs.next()) {
				
				BookForm booking = new BookForm();
				booking.setB_id(rs.getInt(1));
				booking.setF_name(rs.getString(2));
				booking.setL_name(rs.getString(3));
				booking.setcityid(rs.getInt(4));
				booking.setD_to(rs.getString(5));
				booking.setContact(rs.getString(6));
				booking.setCompartment(rs.getString(7));
				
				book.add(booking);
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return book;
		
	}

	public List<BookForm> displayByid(String b_id) {
		
		List<BookForm> book = new ArrayList<BookForm>();
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/bookingsystem", "root", "password");

			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery("select * from bookform where b_id='"+b_id+"'");
			
			
			while(rs.next()) {
				
				BookForm booking = new BookForm();
				booking.setB_id(rs.getInt(1));
				
				book.add(booking);
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return book;
		
	}
	
	public String getcityid(int cityid) {

		return "";
	}

	public String login(String f_name, String l_name) {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/bookingsystem", "root", "password");

			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery("select * from bookform where f_name='"+f_name+"' and l_name='"+l_name+"'");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

}

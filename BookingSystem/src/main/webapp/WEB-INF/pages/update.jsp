<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Booking System</title>
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
	integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N"
	crossorigin="anonymous">
</head>
<body>
<div class="alert alert-success" role="alert">
	<p><h1 align="center">Update page</h1></p>
	</div>
	<%
	String id = request.getParameter("b_id");

	String host = "jdbc:mysql://localhost:3306/bookingsystem";
	Connection con = null;
	Statement st = null;
	ResultSet rs = null;
	Class.forName("com.mysql.cj.jdbc.Driver");
	con = DriverManager.getConnection(host, "root", "password");
	st = con.createStatement();
	String data = "select * from bookform where b_id='" + id + "'";
	rs = st.executeQuery(data);
	while (rs.next()) {
	%>

	<form action="update" method="post">

		<table style="with: 50%" align="center">
			<input type="hidden" name="b_id" value='<%=rs.getString("b_id")%>' />
			
			<tr>
				<td>Compartment</td>
				<td><input value='<%= rs.getString("compartment")%>'/> <select name="compartment">
						<option>General</option>
						<option>Second class</option>
						<option>Super class</option>
						<option>Sleeper class</option>
						<option>AC</option>
				</select>  </td>
			</tr>
			
			<tr>
				<td>First Name</td>
				<td><input type="text" name="f_name"
					value='<%=rs.getString("f_name")%>' /></td>
			</tr>
			<tr>
				<td>Last Name</td>
				<td><input type="text" name="l_name"
					value='<%=rs.getString("l_name")%>' /></td>
			</tr>
			<tr>
				<td>From City</td>
				<td><input type="text" name="cityid"
					value='<%=rs.getString("cityid")%>' /></td>
			</tr>
			<tr>
				<td>To City</td>
				<td><input type="text" name="d_to"
					value='<%=rs.getString("d_to")%>' /></td>
			</tr>
			<tr>
				<td>Contact No</td>
				<td><input type="text" name="contact"
					value='<%=rs.getString("contact")%>' /></td>
			</tr>



			<tr>
				<td></td>
				<td> <input type="submit" value="update" class="btn btn-success" />
					<button type="submit" formaction="/BookingSystem/bookinglist"
						class="btn btn-primary">Back to list</button></td>
			</tr>
		</table>


	</form>
	<%
	}
	%>
</body>
</html>


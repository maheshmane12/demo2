<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>book System</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
<style>
.btn-css{
padding:5px !important;
margin:0px 0px 0px 55px !important;
}
</style>
</head>

<body>
<div class="alert alert-success" role="alert">
        <p><h1 align="center">LOGIN HERE</h1></p>
    </div>

<form action="login">
<table align="center" style="width:30%">
<tr>
<td>Name:</td>
<td><input type="text" name="username"></td>
</tr>

<tr>
<td>Password:</td>
<td><input type="password" name="password"></td>
</tr>

<tr>
<td></td>
<td><input type="submit" name="login" class="btn btn-success"><button type="submit" formaction="/BookingSystem/register" class="btn btn-primary btn-css">Register</button></td>
</tr>
</table>
</form>
</body>
</html>
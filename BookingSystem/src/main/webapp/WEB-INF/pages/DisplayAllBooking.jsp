<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>booking system</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
<style>
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}
td {
  text-align: center;
}
.btn-css{
padding:5px !important;
margin:0px 0px 0px 55px !important;
} 


</style>
</head>
<body>

<div class="alert alert-success" role="alert">
        <p><h1 align="center">ALL CUSTOMER BOOKING LIST</h1></p>
    </div>

<form action="bookinglist" method="post">
<table style="width: 75%;" class="table"  align="center" >
<thead class="thead-dark">
<tr>
<th>b_id</th>
<th>f_name</th>
<th>l_name</th>
<th>cityid</th>
<th>d_to</th>
<th>contact</th>
<th>compartment</th>
<th>Edit</th>
<th>Delete</th>
</tr>
</thead>

<c:forEach  var="list"  items="${booklist}">
<tr>
		<td>${list.b_id} </td>
        <td>${list.f_name} </td>
        <td>${list.l_name} </td>
        <td>${list.cityid} </td>
        <td>${list.d_to} </td>
        <td>${list.contact} </td>
        <td>${list.compartment} </td>
        
<td><a href="update?b_id=${list.b_id}" class="btn btn-warning">Update</a></td>

<td><a href="delete?b_id=${list.b_id}"   class="btn btn-danger">Delete</a></td>
</tr>
</c:forEach> 
</table>
</form>

<!-- <div> -->
<a href="/BookingSystem/book"><button class="btn btn-success btn-css">New Booking</button></a>
<!-- </div> -->


</body>
</html>
package com.booking.controller;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.booking.dao.BookingDao;
import com.booking.model.BookForm;

@Controller
public class BookController {
	
	BookingDao dao = new BookingDao();
	
	@RequestMapping("/login")
	public ModelAndView login(HttpServletRequest req, HttpServletResponse rep) {
		
		String username = req.getParameter("username");
		String password = req.getParameter("password");
		
		boolean uservalid = dao.uservalid(username, password);
		ModelAndView mv = new ModelAndView();
		if(uservalid) {
			mv.setViewName("book");
			return mv;
		}
		
		mv.setViewName("login");
		return mv;
	}
	
	@RequestMapping("/register")
	public ModelAndView accregister(HttpServletRequest req, HttpServletResponse rep) 
	{
		String f_name=req.getParameter("f_name");
		String l_name=req.getParameter("l_name");
		String username = req.getParameter("username");
		String password=req.getParameter("password");
		String email=req.getParameter("email");
		String result =dao.register(f_name, l_name, username, password, email);		
		ModelAndView mv = new ModelAndView();
		return mv;
	}
	
	@RequestMapping("/book")
	public ModelAndView bookform(HttpServletRequest req, HttpServletResponse rep) 
	{
		String f_name=req.getParameter("f_name");
		String l_name=req.getParameter("l_name");
		String cityid = req.getParameter("cityid");
		String d_to=req.getParameter("d_to");
		String contact=req.getParameter("contact");
		String compartment=req.getParameter("compartment");
		String result =dao.insertdata(f_name, l_name, cityid, d_to, contact, compartment);		
		ModelAndView mv = new ModelAndView();
		return mv;
	}

	@RequestMapping("/update")
	public ModelAndView update(HttpServletRequest req, HttpServletResponse rep) throws NumberFormatException, SQLException {

		String f_name=req.getParameter("f_name");
		String l_name=req.getParameter("l_name");
		String cityid = req.getParameter("cityid");
		String d_to=req.getParameter("d_to");
		String contact=req.getParameter("contact");
		String compartment=req.getParameter("compartment");
		String b_id=req.getParameter("b_id");
		
		String result = dao.change(b_id, f_name, l_name, cityid, d_to, contact, compartment);
		System.out.println(result);
		ModelAndView mv = new ModelAndView();
		return mv;
	}
	

	
	@RequestMapping("/delete")
	public ModelAndView deleteById(HttpServletRequest req, HttpServletResponse rep) {
		
		String b_id = req.getParameter("b_id");
		System.out.println(b_id);
		String result = dao.delete(b_id);
		
		ModelAndView mv = new ModelAndView();
		return mv;
	}
	
	@RequestMapping("/bookinglist")
	public ModelAndView showTable(){
		 List<BookForm> bookinglist= dao.display();
		 
		 ModelAndView model = new ModelAndView();
		 
	        model.addObject("booklist",bookinglist);
	        model.setViewName("DisplayAllBooking");
	        return model; 
	}
	

	
}

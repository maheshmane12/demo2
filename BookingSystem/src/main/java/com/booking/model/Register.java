package com.booking.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "register")
public class Register {

	@Id
	@GeneratedValue
	private int id;
	private String f_name;
	private String l_name;
	private String username;
	private String password;
	private String email;

	public Register() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Register(int id, String f_name, String l_name, String username, String password, String email) {
		super();
		this.id = id;
		this.f_name = f_name;
		this.l_name = l_name;
		this.username = username;
		this.password = password;
		email = email;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getF_name() {
		return f_name;
	}

	public void setF_name(String f_name) {
		this.f_name = f_name;
	}

	public String getL_name() {
		return l_name;
	}

	public void setL_name(String l_name) {
		this.l_name = l_name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Register [id=" + id + ", f_name=" + f_name + ", l_name=" + l_name + ", username=" + username
				+ ", password=" + password + ", email=" + email + "]";
	}

}

<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>booking system</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
<style>
.div-css{
border: 2px solid;
width:40%;
margin:auto;
padding-top:15px;
padding-bottom:15px;
}
</style>

</head>
<body>
<div class="alert alert-success" role="alert">
        <p><h1 align="center">REGISTER HERE</h1></p>
    </div>
	<div class="div-css">
	<form action="register" method="post">
	<table align="center">
			<tr>
				<td>First Name</td>
				<td><input type="text" name="f_name" /></td>
			</tr>
			<tr>
				<td>Last Name</td>
				<td><input type="text" name="l_name" /></td>
			</tr>
						<tr>
				<td>Username</td>
				<td><input type="text" name="username" /></td>
			</tr>
			<tr>
				<td>Password</td>
				<td><input type="password" name="password" /></td>
			</tr>
						<tr>
				<td>Email</td>
				<td><input type="text" name="email" /></td>
			</tr>
			
			<tr>
			<td></td>
			<td><input type="submit" value="save" class="btn btn-success"></td>
			</tr>
	</table>
	</form>
	</div>
</body>
</html>

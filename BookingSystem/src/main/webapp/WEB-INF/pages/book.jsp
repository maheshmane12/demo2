<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>booking system</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
</head>
<body>
<div class="alert alert-success" role="alert">
	<p><h1 align="center">Booking Page</h1></p>
	</div>
	<form action="book" method="post">
		<table style="with: 50%" align="center">
		
		<tr>
		<td>Compartment:</td>
		<td>
			<select name="compartment">
				<option>General</option>
				<option>Second class</option>
				<option>Super class</option>
				<option>Sleeper class</option>
				<option>AC</option>
			</select>
			</td>
</tr>
			<tr>
				<td>First Name</td>
				<td><input type="text" name="f_name" /></td>
			</tr>
			<tr>
				<td>Last Name</td>
				<td><input type="text" name="l_name" /></td>
			</tr>
			<tr>
				<td>From City</td>
				<td>
				<select name="cityid">
				<option>Select City</option>
					<%
					String host = "jdbc:mysql://localhost:3306/bookingsystem";
					Connection con = null;
					Statement st = null;
					ResultSet rs = null;
					Class.forName("com.mysql.cj.jdbc.Driver");
					con = DriverManager.getConnection(host, "root", "password");
					st = con.createStatement();
					String data = "select * from city";
					rs = st.executeQuery(data);
					while(rs.next()){
						%>
						<option><%= rs.getString("cityname") %></option>
					
					<% }%>
				</select>
				</td>
			<tr>
				<td>To City</td>
				<td><input type="text" name="d_to" /></td>
			</tr>
			<tr>
				<td>Contact No</td>
				<td><input type="text" name="contact" /></td>
			</tr>

<tr>
<td></td>
<td><input type="submit" value="Submit" class="btn btn-success"/>
		 <button type="submit" formaction="/BookingSystem/bookinglist" class="btn btn-primary">Booking
			List</button></td>
</tr>
		</table>

		

	</form>

</body>
</html>
